(function($) {
    var digits = "0123456789";
    var handlerId = "numberInput-handler";
    var optionsId = "numberInput-options";
    var events = "input change";
    
    var testNumber = function(input) {
        var caretPos = input.selectionStart;
        
        var $input = $(input);
        var settings = $input.data(optionsId);
        var text = $input.val();
        
        if (text.length > 0) {
            var intPart = "";
            var floatPart = "";
            var dotPassed = false;
            var minusExists = false;

            for (var chIndex in text) {
                var ch = text[chIndex];
                if ((chIndex == 0) && (ch == '-')) {
                    minusExists = true;
                } else if (ch == "." || ch == "," ) {
                    dotPassed = true;
                } else if (digits.indexOf(ch) > -1) {
                    if (dotPassed) {
                        floatPart = floatPart + ch;
                    } else {
                        intPart = intPart + ch;
                    }
                }
            }

            if (intPart.length > settings.intDigitCount) {
                if (settings.intDigitCount > 0) {
                    intPart = intPart.substring(0, settings.intDigitCount);
                }
            }
            if (floatPart.length > settings.floatDigitCount) {
                floatPart = floatPart.substring(0, settings.floatDigitCount);
            }

            var parsedText = "";
            if ((settings.mayBeNegative) && minusExists) {
                parsedText = "-";
            }
            if (intPart.length == 0) {
                parsedText = parsedText + "0";
            } else {
                parsedText = parsedText + intPart;
            }
            if (settings.mayBeFloat && dotPassed) {
                parsedText = parsedText + ".";
                if (floatPart.length > 0) {
                    parsedText = parsedText + floatPart;
                }
            }

            if (parsedText != text) {
                $input.val(parsedText);
                if (caretPos > 0) {
                    caretPos = caretPos - 1;
                }
                try {
                    input.setSelectionRange(caretPos, caretPos);
                } catch(e) {}
            }
        }
    };
    
    $.fn.numberInput = function(settings) {
        return this.each(function() {
            var options = $.extend({
                intDigitCount: 8,
                floatDigitCount: 2,
                mayBeNegative: true,
                mayBeFloat: true
            }, settings, true);
            
            var $this = $(this);
            options.elementId = $this.attr("id");
            $this.data(optionsId, options);
            
            var handler = $this.data(handlerId);
            if (handler != null) {
                $this.unbind(events, handler);
            }
            
            handler = function() {
                testNumber(this);
            };
            
            $this.data(handlerId, handler);
            
            testNumber(this);
            
            $this.bind(events, handler);
        });
    };
    
    $.fn.noNumberInput = function() {
        return this.each(function() {
            var $this = $(this);
            var handler = $this.data(handlerId);
            if (handler != null) {
                $this.unbind(events, handler);
            }
        });
    };
})(jQuery);