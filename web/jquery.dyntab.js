var itemUp = function(array, index) {
    if (index > 0) {
        var result = [];
        for (var thisIndex in array) {
            if (thisIndex == (index - 1)) {
                result.push(array[index]);
            } else if (thisIndex == index) {
                result.push(array[index - 1])
            } else {
                result.push(array[thisIndex]);
            }
        }
        return result;
    } else {
        return array;
    }
};

var itemDown = function(array, index) {
    if (index < (array.length - 1)) {
        var result = [];
        for (var thisIndex in array) {
            if (thisIndex == (index + 1)) {
                result.push(array[index]);
            } else if (thisIndex == index) {
                result.push(array[index + 1])
            } else {
                result.push(array[thisIndex]);
            }
        }
        return result;
    } else {
        return array;
    }
};

var itemDelete = function(array, index) {
    var result = [];

    for (var thisIndex in array) {
        if (thisIndex != index) {
            result.push(array[thisIndex]);
        }
    }

    return result;
};

(function($) {
    // пространство имен
    var namespace = "dyntab.";
    var dataRowIndexParam = namespace + "dataRowIndex";
    var dataCellIndexParam = namespace + "dataCellIndex";

    var datepickerSettings = {
        autoSize: true, // подгоняет размер поля ввода под формат даты
        changeMonth: true, // возможность выбрать месяц
        changeYear: true, // возможность выбрать год
        constrainInput: true, // не дает вводить символы, которые не подходят по формату
        dateFormat: "dd.mm.yy", // формат даты
        firstDay: 1, // первый день - понедельник (воскресенье - 0)
        navigationAsDateFormat: true, // навигация согласно формату даты (надо глянуть, что за чудо-навигация)
        showButtonPanel: true, // показ кнопков
        showWeek: true, // показ колонки недели
        yearRange: "1900:2030"
    };

    var createDictField = function(container, list, level, selected) {
        var $container = $(container);

        for (var listIndex in list) {
            var listItem = list[listIndex];
            if (listItem != null) {
                var containerItem = null;
                var title = "" + listItem.title;
                if ((listItem.children != null) && (listItem.children.length > 0)) {
                    // optgroup
                    for (var i = 0; i < level; i++) {
                        title = "- " + title;
                    }
                    containerItem = $("<optgroup></optgroup>");
                    containerItem
                            .attr("label", title)
                            .appendTo($container);

                    createDictField(container, listItem.children, level + 1, selected);
                } else {
                    // option
                    for (var i = 0; i < level; i++) {
                        title = "- " + title;
                    }
                    containerItem = $("<option></option>");
                    containerItem
                            .attr("value", listItem.id)
                            .html(title)
                            .appendTo($container);

                    if (selected == null) {
                        if (listItem.id == null) {
                            containerItem
                                    .attr("selected", true);
                        }
                    } else if (selected == listItem.id) {
                        containerItem
                                .attr("selected", true);
                    }
                }
            }
        }
    }

    var createTable = function(container, settings) {
        var $container = $(container);
        $container.empty();

        // создание таблицы
        var table = $("<table></table>");
        table
                .css(settings.styles.table.css)
                .addClass(settings.styles.table.addClass)
                .appendTo($container);
        // Создание строки заголовков
        var headerRow = $("<tr></tr>");
        headerRow
                .css(settings.styles.headerRow.css)
                .addClass(settings.styles.headerRow.addClass)
                .appendTo(table);

        // Применение настроек
        if ((settings != null) && (settings.header != null) && (settings.header.length > 0)) {

            // Добавление ячеек заголовков действий
            if (!settings.readOnly){
                $("<th style=\"padding: 5px;\"></th>")
                        .css(settings.styles.headerCell.css)
                        .addClass(settings.styles.headerCell.addClass)
                        .html(settings.buttons.direction)
                        .appendTo(headerRow);
                $("<th style=\"padding: 5px;\"></th>")
                        .css(settings.styles.headerCell.css)
                        .addClass(settings.styles.headerCell.addClass)
                        .html(settings.buttons.manipulations)
                        .appendTo(headerRow);
            }
            // Добавление ячеек заголовков
            for (var headerIndex in settings.header) {
                var headerItem = settings.header[headerIndex];
                var headerCell = $("<th style=\"padding: 5px;\"></th>");
                settings.header[headerIndex].cell = headerCell;
                headerCell
                        .css(settings.styles.headerCell.css)
                        .addClass(settings.styles.headerCell.addClass)
                        .html(headerItem.title)
                        .appendTo(headerRow);
            }

            // Добавление данных
            if ((settings.data != null) && (settings.data.length > 0)) {
                for (var dataRowIndex in settings.data) {
                    var dataRow = settings.data[dataRowIndex];
                    var tableDataRow = $("<tr></tr>");
                    tableDataRow
                            .css(settings.styles.row.css)
                            .addClass(settings.styles.row.addClass)
                            .appendTo(table);
                    if (!settings.readOnly) {
                        var tableDataCell = $("<td></td>");
                        tableDataCell
                                .css(settings.styles.cell.css)
                                .addClass(settings.styles.cell.addClass)
                                .appendTo(tableDataRow);

                        // кнопка "Вверх"
                        if (dataRowIndex > 0) {
                            var button = $("<input type=\"button\"/>");
                            button
                                    .attr("value", settings.buttons.up)
                                    .data(dataRowIndexParam, dataRowIndex)
                                    .css(settings.styles.button.css)
                                    .addClass(settings.styles.button.addClass)
                                    .bind("click", function() {
                                        settings.data = itemUp(settings.data, parseInt($(this).data(dataRowIndexParam)));
                                        createTable(container, settings);
                                    })
                                    .appendTo(tableDataCell);
                        }

                        // кнопка "Вниз"
                        if (dataRowIndex < (settings.data.length - 1)) {
                            if (dataRowIndex > 0) {
                                $("<br/>").appendTo(tableDataCell);
                            }
                            var button = $("<input type=\"button\"/>");
                            button
                                    .attr("value", settings.buttons.down)
                                    .data(dataRowIndexParam, dataRowIndex)
                                    .css(settings.styles.button.css)
                                    .addClass(settings.styles.button.addClass)
                                    .bind("click", function() {
                                        settings.data = itemDown(settings.data, parseInt($(this).data(dataRowIndexParam)));
                                        createTable(container, settings);
                                    })
                                    .appendTo(tableDataCell);
                        }

                        // кнопка "Удалить"
                        var tableDataCell = $("<td></td>");
                        tableDataCell
                                .css(settings.styles.cell.css)
                                .addClass(settings.styles.cell.addClass)
                                .appendTo(tableDataRow);
                        var button = $("<input type=\"button\"/>");
                        button
                                .attr("value", settings.buttons.del)
                                .data(dataRowIndexParam, dataRowIndex)
                                .css(settings.styles.button.css)
                                .addClass(settings.styles.button.addClass)
                                .bind("click", function() {
                                    settings.data = itemDelete(settings.data, parseInt($(this).data(dataRowIndexParam)));
                                    createTable(container, settings);
                                })
                                .appendTo(tableDataCell);
                    }


                    if ((dataRow != null) && (dataRow.length > 0)) {
                        for (var dataCellIndex in dataRow) {
                            var dataCell = dataRow[dataCellIndex];
                            var tableDataCell = $("<td></td>");
                            tableDataCell
                                    .css(settings.styles.cell.css)
                                    .addClass(settings.styles.cell.addClass)
                                    .appendTo(tableDataRow);
                            if (settings.header.length > dataCellIndex) {
                                var headerItem = settings.header[dataCellIndex];
                                var tableDataCellComponent = null;
                                switch (headerItem.type) {
                                    case "n":
                                        tableDataCellComponent = $("<input type=\"text\"/>");
                                        tableDataCellComponent
                                                .data(dataRowIndexParam, dataRowIndex)
                                                .data(dataCellIndexParam, dataCellIndex)
                                                .attr("name", settings.fieldPrefix + "-" + dataRowIndex + "-" + headerItem.columnId)
                                                .css(settings.styles.numberInput.css)
                                                .addClass(settings.styles.numberInput.addClass)
                                                .val(dataCell)
                                                .numberInput(headerItem.format)
                                                .bind("change", function() {
                                                    var $this = $(this);
                                                    settings.data[$this.data(dataRowIndexParam)][$this.data(dataCellIndexParam)] = $(this).val();
                                                });
                                        break;
                                    case "d":
                                        tableDataCellComponent = $("<input type=\"text\"/>");
                                        tableDataCellComponent
                                                .data(dataRowIndexParam, dataRowIndex)
                                                .data(dataCellIndexParam, dataCellIndex)
                                                .attr("name", settings.fieldPrefix + "-" + dataRowIndex + "-" + headerItem.columnId)
                                                .css(settings.styles.dateInput.css)
                                                .addClass(settings.styles.dateInput.addClass)
                                                .val(dataCell)
                                                .datepicker(datepickerSettings)
                                                .bind("change", function() {
                                                    var $this = $(this);
                                                    settings.data[$this.data(dataRowIndexParam)][$this.data(dataCellIndexParam)] = $(this).val();
                                                });
                                        break;
                                    case "b":
                                        tableDataCellComponent = $("<input type=\"checkbox\"/>");
                                        tableDataCellComponent
                                                .data(dataRowIndexParam, dataRowIndex)
                                                .data(dataCellIndexParam, dataCellIndex)
                                                .attr("name", settings.fieldPrefix + "-" + dataRowIndex + "-" + headerItem.columnId)
                                                .css(settings.styles.booleanInput.css)
                                                .addClass(settings.styles.booleanInput.addClass)
                                                .bind("change", function() {
                                                    var $this = $(this);
                                                    settings.data[$this.data(dataRowIndexParam)][$this.data(dataCellIndexParam)] = $(this).prop("checked");
                                                });
                                        if (dataCell) {
                                            tableDataCellComponent.attr("checked", true);
                                        }
                                        break;
                                    case "c":
                                        tableDataCellComponent = $("<select></select>");
                                        tableDataCellComponent
                                                .data(dataRowIndexParam, dataRowIndex)
                                                .data(dataCellIndexParam, dataCellIndex)
                                                .attr("name", settings.fieldPrefix + "-" + dataRowIndex + "-" + headerItem.columnId)
                                                .css(settings.styles.dictionaryInput.css)
                                                .addClass(settings.styles.dictionaryInput.addClass)
                                                .bind("change", function() {
                                                    var $this = $(this);
                                                    settings.data[$this.data(dataRowIndexParam)][$this.data(dataCellIndexParam)] = $this.children(":selected").attr("value");
                                                });
                                        createDictField(tableDataCellComponent, headerItem.dict, 0, dataCell);
                                        break;
                                    case "s":
                                        tableDataCellComponent = $("<input type=\"text\"/>");
                                        tableDataCellComponent
                                                .data(dataRowIndexParam, dataRowIndex)
                                                .data(dataCellIndexParam, dataCellIndex)
                                                .attr("name", settings.fieldPrefix + "-" + dataRowIndex + "-" + headerItem.columnId)
                                                .css(settings.styles.textInput.css)
                                                .addClass(settings.styles.textInput.addClass)
                                                .val(dataCell)
                                                .bind("change", function() {
                                                    var $this = $(this);
                                                    settings.data[$this.data(dataRowIndexParam)][$this.data(dataCellIndexParam)] = $(this).val();
                                                });
                                        break;
                                    case "m":
                                        tableDataCellComponent = $("<textarea></textarea>");
                                        tableDataCellComponent
                                                .data(dataRowIndexParam, dataRowIndex)
                                                .data(dataCellIndexParam, dataCellIndex)
                                                .attr("name", settings.fieldPrefix + "-" + dataRowIndex + "-" + headerItem.columnId)
                                                .css(settings.styles.multilineInput.css)
                                                .addClass(settings.styles.multilineInput.addClass)
                                                .val(dataCell)
                                                .bind("change", function() {
                                                    var $this = $(this);
                                                    settings.data[$this.data(dataRowIndexParam)][$this.data(dataCellIndexParam)] = $(this).val();
                                                });
                                        break;
                                    default:
                                        break;
                                }
                                if (tableDataCellComponent != null) {
                                    if (settings.readOnly) {
                                        tableDataCellComponent.attr("disabled", "true");
                                    }
                                    tableDataCellComponent
                                            .appendTo(tableDataCell);
                                }
                            }
                        }

                        if (!settings.readOnly) {
                            var tableDataCell = $("<td></td>");
                            tableDataCell
                                    .css(settings.styles.cell.css)
                                    .addClass(settings.styles.cell.addClass)
                                    .appendTo(tableDataRow);

                            // кнопка "Вверх"
                            if (dataRowIndex > 0) {
                                var button = $("<input type=\"button\"/>");
                                button
                                        .attr("value", settings.buttons.up)
                                        .data(dataRowIndexParam, dataRowIndex)
                                        .css(settings.styles.button.css)
                                        .addClass(settings.styles.button.addClass)
                                        .bind("click", function() {
                                            settings.data = itemUp(settings.data, parseInt($(this).data(dataRowIndexParam)));
                                            createTable(container, settings);
                                        })
                                        .appendTo(tableDataCell);
                            }

                            // кнопка "Вниз"
                            if (dataRowIndex < (settings.data.length - 1)) {
                                if (dataRowIndex > 0) {
                                    $("<br/>").appendTo(tableDataCell);
                                }
                                var button = $("<input type=\"button\"/>");
                                button
                                        .attr("value", settings.buttons.down)
                                        .data(dataRowIndexParam, dataRowIndex)
                                        .css(settings.styles.button.css)
                                        .addClass(settings.styles.button.addClass)
                                        .bind("click", function() {
                                            settings.data = itemDown(settings.data, parseInt($(this).data(dataRowIndexParam)));
                                            createTable(container, settings);
                                        })
                                        .appendTo(tableDataCell);
                            }

                            // кнопка "Удалить"
                            var tableDataCell = $("<td></td>");
                            tableDataCell
                                    .css(settings.styles.cell.css)
                                    .addClass(settings.styles.cell.addClass)
                                    .appendTo(tableDataRow);
                            var button = $("<input type=\"button\"/>");
                            button
                                    .attr("value", settings.buttons.del)
                                    .data(dataRowIndexParam, dataRowIndex)
                                    .css(settings.styles.button.css)
                                    .addClass(settings.styles.button.addClass)
                                    .bind("click", function() {
                                        settings.data = itemDelete(settings.data, parseInt($(this).data(dataRowIndexParam)));
                                        createTable(container, settings);
                                    })
                                    .appendTo(tableDataCell);
                        }
                    }
                }
            }

            if (!settings.readOnly) {
                // кнопка "Добавить"
                var newRow = $("<tr></tr>")
                        .css(settings.styles.row.css)
                        .addClass(settings.styles.row.addClass)
                        .appendTo(table);
                var newCell = $("<td></td>")
                        .css(settings.styles.cell.css)
                        .addClass(settings.styles.cell.addClass)
                        .appendTo(newRow);
                newCell = $("<td></td>")
                        .attr("colspan", settings.header.length + 1)
                        .css(settings.styles.cell.css)
                        .addClass(settings.styles.cell.addClass)
                        .appendTo(newRow);
                var newButton = $("<input type=\"button\"/>")
                        .attr("value", settings.buttons.add)
                        .css(settings.styles.button.css)
                        .addClass(settings.styles.button.addClass)
                        .appendTo(newCell)
                        .bind("click", function() {
                            var row = [];
                            for (var headerIndex in settings.header) {
                                var headerItem = settings.header[headerIndex];
                                switch (headerItem.type) {
                                    case "c":
                                        row.push(null);
                                        break;
                                    default:
                                        row.push("");
                                        break;
                                }
                            }
                            settings.data.push(row);
                            createTable(container, settings);
                        });
            }
        }
    };

    $.fn.dyntab = function(options) {
        var settings = $.extend(true, {
            readOnly: false,
            fieldPrefix: "field-123",
            buttons: {
                up: "Up",
                down: "Down",
                del: "Delete",
                add: "Add",
                direction: "Direction",
                manipulations: "Manipulations"
            },
            header: [],
            data: [],
            styles: {
                table: {
                    css: {},
                    addClass: ""
                },
                headerRow: {
                    css: {},
                    addClass: ""
                },
                headerCell: {
                    css: {},
                    addClass: ""
                },
                row: {
                    css: {},
                    addClass: ""
                },
                cell: {
                    css: {},
                    addClass: ""
                },
                numberInput: {
                    css: {},
                    addClass: ""
                },
                dateInput: {
                    css: {},
                    addClass: ""
                },
                booleanInput: {
                    css: {},
                    addClass: ""
                },
                dictionaryInput: {
                    css: {},
                    addClass: ""
                },
                textInput: {
                    css: {},
                    addClass: ""
                },
                multilineInput: {
                    css: {},
                    addClass: ""
                },
                button: {
                    css: {},
                    addClass: ""
                }
            }
        }, options);

        if (settings.header.length == 0) {
            return $(this);
        } else {
            return $(this).each(function() {
                createTable(this, settings);
            });
        }
    };


})(jQuery);

function checkEmptyRequredLogiacl() {
    var tr = $("tr.required");
    for (i = 0; i < $(tr).length; i++) {
        var td = $(tr[i]).children("td");
        var ul = $(td[1]).children("ul");
        if ($(ul).length > 0) {
            checked = $(ul).find("input:checked");
            if ($(checked).length === 0) {
                //console.log($(td[0]).val());
                $(td[1]).addClass("requredlogic");
                //$(ul).find("input").on("click", checkEmptyRequredLogiacl);
                $((ul).find("input:first")[0]).get(0).scrollIntoView();

                return;
            } else
                $(td[1]).removeClass("requredlogic");
        }

        tab = $(td[1]).children("table");

        if ($(tab).length > 0) {

            checked = $(tab).find("tr");
            if ((tab).attr("id") != "data") {
                if ($(checked).length < 3) {
                    $(td[1]).addClass("requredlogic");
                    //$(tab).find("input").on("click", checkEmptyRequredLogiacl);
                    $((tab).find("input:first")[0]).get(0).scrollIntoView();

                    return;
                } else
                    $(td[1]).removeClass("requredlogic");
            }
        }

    }
}
;

var changeTooltipPosition = function(event) {
    var tooltipX = event.pageX - 8;
    var tooltipY = event.pageY + 8;
    $('div.tooltip').css({top: tooltipY, left: tooltipX});
};

var showTooltip = function(td) {
    $('div.tooltip').remove();
    $('<div class="tooltip">Заполните одно из полей</div>')
            .appendTo('body');
    changeTooltipPosition(td);
};

var hideTooltip = function() {
    $('div.tooltip').remove();
};




function showtool(td) {

    $(td).bind({
        mousemove: changeTooltipPosition,
        mouseenter: showTooltip,
        mouseleave: hideTooltip
    });
}
;

function onsubmit() {
    setTimeout(function() {
        $("#mainsubmit").show(5000);
        $("#mainsubmit1").show(5000);
    }, 5000);
}
function click() {
    $("#mainsubmit").hide(300);
    $("#mainsubmit1").hide(300);
    /*setTimeout(function() {
        $("#mainsubmit").show(5000);
        $("#mainsubmit1").show(5000);
    }, 5000);*/
}