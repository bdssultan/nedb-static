﻿(function($) {
    var regexpParam = "inputmask.regexp";
    var prevValueParam = "inputmask.prevValue";
    var messageParam = "inputmask.message";
    
    $.fn.inputmask = function(regexp, message) {
        return this.each(function() {
            var value = $(this).val();
            if (!regexp.test(value)) {
                value = "";
            }
            $(this).data(regexpParam, regexp)
                .data(prevValueParam, value)
                .data(messageParam, message)
                .bind("change", function() {
                    var $this = $(this);
                    var regexp = $this.data(regexpParam);
                    var value = $this.val();
                    
                    var result = regexp.test(value);
                    if (!result) {
                        result = regexp.test(value);
                    }
                    
                    if (result || (value.length == 0)) {
                        $this.data(prevValueParam, value);
                    } else {
                        //alert(value + "\n" + regexp + "\n" + result + "\n\n" + $this.data(messageParam));
                        alert($this.data(messageParam));
                        $this.val($this.data(prevValueParam));
                    }
                });
        });
    };
})(jQuery);