/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {

    $('#table_reg').dataTable({
        "language": {
            "lengthMenu": "Показать _MENU_ записей на странице",
            "zeroRecords": "К сожелению ничего не найдено",
            "info": "Показаны с _START_ по _END_ записей из _TOTAL_",
            "infoEmpty": "Не загружено данных",
            "search": "Поиск по всему реестру",
            "paginate": {
                "next": "Следующая",
                "previous": "Предыдущая"
            },
            "infoFiltered": "(найдено из _MAX_ записей)"
        },
        "lengthMenu": [[50, 100, 200, -1], [50, 100, 200, "Все"]],
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/static/TableTools/swf/copy_csv_xls_pdf.swf"
        }
    });

    
    var table = $('#table_reg').DataTable();
    $('#table_reg tfoot th').each(function() {
        var title = $('#table_reg thead th').eq($(this).index()).text();
        $(this).html('<input type="text" placeholder="Искать по ' + title + '" />');
    });

    table.columns().eq(0).each(function(colIdx) {
        $('input', table.column(colIdx).footer()).on('keyup change', function() {
            table
                    .column(colIdx)
                    .search(this.value)
                    .draw();
        });
    });
})