(function($) {
    $.fn.nedbAutocomplete = function(settings) {
        var interfaceDelay = 250;

        var dataPrefix = "nedb-autocomplete";
        var dataOptions = dataPrefix + "-options";
        var dataVariantWindow = dataPrefix + "-variant-window";
        var dataVariant = dataPrefix + "-variant";
        var dataChoosenVariant = dataPrefix + "-choosen-variant";
        var dataInput = dataPrefix + "-input";
        var dataTimeoutId = dataPrefix + "-timeoutId";
        var dataLoadThreads = dataPrefix + "-load-threads";
        var dataLabelLoading = dataPrefix + "-label-loading";
        var dataSearchedTexts = dataPrefix + "-searched-texts";

        var sampleString = "";
        var sampleObject = {};
        var sampleArray = [];
        var sampleFunction = function() {};
        var sampleInt = 0;

        var isString = function(object) {
            if (object == null) {
                return false;
            } else {
                return (typeof object == typeof sampleString);
            }
        }

        var isStringWithText = function(object) {
            if (!isString(object)) {
                return false;
            } else {
                return (object.length > 0);
            }
        }

        var isObject = function(object) {
            if (object == null) {
                return false;
            } else {
                return (typeof object == typeof sampleObject);
            }
        }

        var isArray = function(object) {
            if (object == null) {
                return false;
            } else {
                return (typeof object == typeof sampleArray);
            }
        }

        var isArrayWithItems = function(object) {
            if (isArray(object)) {
                return (object.length > 0);
            } else {
                return false;
            }
        }

        var isObjectWithFields = function(object, fields) {
            if (!isObject(object) || !isArrayWithItems(fields)) {
                return false;
            } else {
                for (var fieldId in fields) {
                    var field = fields[fieldId];
                    if (!isStringWithText(field)) {
                        return false;
                    } else if (object[field] == undefined) {
                        return false;
                    }
                }

                return true;
            }
        }

        var isFunction = function(object) {
            if (object == null) {
                return false;
            } else {
                return (typeof object == typeof sampleFunction);
            }
        }

        var isInt = function(object) {
            if (object == null) {
                return false;
            } else {
                return (typeof object == typeof sampleInt);
            }
        }

        var options = $.extend({
            items:[],
            checkItemField: "text",
            ajax: null,
            ajaxParam: null,
            labelLoading: "Loading",
            startLoadingDelay: 1000,
            itemChoosen: null,
            cleared: null
        }, settings, true);

        if (!isStringWithText(options.labelLoading)) {
            options.labelLoading = "Loading";
        }
        if (!isInt(options.startLoadingDelay)) {
            options.startLoadingDelay = 1000;
        } else if (options.startLoadingDelay < 250) {
            options.startLoadingDelay = 250;
        }

        var updateVariantsWindow = function(autoCompleteInput) {
            var $this = $(autoCompleteInput);

            var searchedText = $this.attr("value");
            if (isString(searchedText)) {
                if (searchedText.length > 0) {
                    searchedText = searchedText.toLowerCase();
                }
            } else {
                searchedText = "";
            }

            var variantsWindow = $this.data(dataVariantWindow);
            if (variantsWindow != null) {
                variantsWindow.variants = [];
                variantsWindow.list.children().remove();

                if (isArrayWithItems(options.items)) {
                    for (var itemId in options.items) {
                        var item = options.items[itemId];
                        if (item != null) {
                            var text = item[options.checkItemField];
                            if (isString(text) && (!isStringWithText(searchedText) || (text.toLowerCase().indexOf(searchedText) != -1))) {
                                variantsWindow.variants.push(item);
                            }
                        }
                    }
                }

                if (variantsWindow.variants.length > 0) {
                    for (var variantId in variantsWindow.variants) {
                        var variant = variantsWindow.variants[variantId];
                        var listItem = $("<li></li>")
                            .appendTo(variantsWindow.list)
                            .data(dataVariant, variant)
                            .data(dataInput, $this)
                            .css({
                                "background-color": "#FFFFFF",
                                "cursor": "pointer",
                                "margin": "5px"
                            })
                            .hover(function() {
                                var $this = $(this);
                                $this.css({
                                    "background-color": "#D0D0D0"
                                });

                                var autocompleteInput = $this.data(dataInput);
                                var variant = $this.data(dataVariant);
                                autocompleteInput.data(dataChoosenVariant, variant);
                            }, function() {
                                $(this).css({
                                    "background-color": "#FFFFFF"
                                });
                            });

                        if (searchedText.length == 0) {
                            listItem.append(variant[options.checkItemField]);
                        } else {
                            var variantText = variant[options.checkItemField];
                            var searchedTextIndex = variantText.toLowerCase().indexOf(searchedText);

                            var firstPart = variantText.substr(0, searchedTextIndex);
                            var middlePart = variantText.substr(searchedTextIndex, searchedText.length);
                            var lastPart = variantText.substr(searchedTextIndex + searchedText.length);

                            listItem
                                .append(firstPart + "<b>" + middlePart + "</b>" + lastPart);
                        }
                    }
                }
            }
        }

        var loadItems = function(autocompleteInput) {
            if (autocompleteInput != null) {
                var options = autocompleteInput.data(dataOptions);
                if (options != null) {
                    if ((options.ajax != null) && isStringWithText(options.ajaxParam)) {
                        var labelLoading = autocompleteInput.data(dataLabelLoading);
                        if (labelLoading != null) {
                            labelLoading.show(250);
                        }

                        var searchedText = autocompleteInput.attr("value");
                        if (isString(searchedText)) {
                            if (searchedText.length > 0) {
                                searchedText = searchedText.toLowerCase();
                            }
                        } else {
                            searchedText = "";
                        }

                        if (!isStringWithText(searchedText)) {
                            var loadingThreads = autocompleteInput.data(dataLoadThreads);
                            if (!isArrayWithItems(loadingThreads)) {
                                var labelLoading = autocompleteInput.data(dataLabelLoading);
                                if (labelLoading != null) {
                                    labelLoading.hide(interfaceDelay);
                                }
                            }
                            return;
                        }

                        var loadingThread = {
                            autocompleteInput: autocompleteInput,
                            canceled: false,
                            searchedText: searchedText
                        };

                        var data = {};
                        if (isObject(options.ajax) && isObject(options.ajax.data)) {
                            data = options.ajax.data;
                        }
                        data[options.ajaxParam] = searchedText;

                        var ajax = $.extend(
                            options.ajax,
                            {
                                data: data,
                                context: loadingThread,
                                complete: function(jqXHR, textStatus) {
                                    var autocompleteInput = this.autocompleteInput;

                                    var loadingThreads = autocompleteInput.data(dataLoadThreads);
                                    if (isArrayWithItems(loadingThreads)) {
                                        var currentIndex = loadingThreads.indexOf(this, 0);
                                        if (currentIndex != -1) {
                                            loadingThreads.splice(currentIndex, 1);
                                            if (loadingThreads.length == 0) {
                                                var labelLoading = autocompleteInput.data(dataLabelLoading);
                                                if (labelLoading != null) {
                                                    labelLoading.hide(interfaceDelay);
                                                }
                                            }
                                        }
                                    }

                                    if (this.canceled) {
                                        var searchedTexts = autocompleteInput.data(dataSearchedTexts);
                                        if (isArrayWithItems(searchedTexts)) {
                                            var searchedTextIndex = null;
                                            for (var searchedTextId in searchedTexts) {
                                                if (searchedTexts[searchedTextId] == this.searchedText) {
                                                    searchedTextIndex = searchedTextId;
                                                    break;
                                                }
                                            }

                                            if (searchedTextIndex != null) {
                                                searchedTexts.splice(searchedTextIndex, 1);
                                                autocompleteInput.data(dataSearchedTexts, searchedTexts);
                                            }
                                        }
                                    }
                                },
                                success: function(data, textStatus, jqXHR) {
                                    if (this.canceled, isArrayWithItems(data)) {
                                        var autocompleteInput = this.autocompleteInput;

                                        var options = autocompleteInput.data(dataOptions);
                                        if (options != null) {
                                            var checkItemField = options.checkItemField;
                                            if (isStringWithText(checkItemField)) {
                                                var loadedItems = [];
                                                if (!isArray(options.items)) {
                                                    options.items = [];
                                                }
                                                var checkExistence = isArrayWithItems(options.items);

                                                for (var loadedItemId in data) {
                                                    if (this.canceled) {
                                                        return;
                                                    }
                                                    var loadedItem = data[loadedItemId];
                                                    if (isObjectWithFields(loadedItem, [checkItemField])) {
                                                        var loadedItemText = loadedItem[checkItemField];
                                                        if (isStringWithText(loadedItemText)) {
                                                            if (checkExistence) {
                                                                var exists = false;

                                                                if (isArrayWithItems(options.items)) {
                                                                    for (var existItemId in options.items) {
                                                                        if (this.canceled) {
                                                                            return;
                                                                        }
                                                                        var existItem = options.items[existItemId];
                                                                        if (isObjectWithFields(existItem, [checkItemField])) {
                                                                            var existItemText = existItem[checkItemField];
                                                                            if (existItemText == loadedItemText) {
                                                                                exists = true;
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                if (!exists) {
                                                                    loadedItems.push(loadedItem);
                                                                }
                                                            } else {
                                                                loadedItems.push(loadedItem);
                                                            }
                                                        }
                                                    }
                                                }

                                                if (!this.cancenled && isArrayWithItems(loadedItems)) {
                                                    options.items = options.items.concat(loadedItems);
                                                    updateVariantsWindow(autocompleteInput);
                                                }
                                            }
                                        }
                                    }
                                }
                            },
                            true
                        );

                        var searchedTexts = autocompleteInput.data(dataSearchedTexts);
                        if (!isArray(searchedTexts)) {
                            searchedTexts = [];
                        }

                        if (isArrayWithItems(searchedTexts)) {
                            for (var searchedTextId in searchedTexts) {
                                if (searchedText == searchedTexts[searchedTextId]) {
                                    var loadingThreads = autocompleteInput.data(dataLoadThreads);
                                    if (!isArrayWithItems(loadingThreads)) {
                                        var labelLoading = autocompleteInput.data(dataLabelLoading);
                                        if (labelLoading != null) {
                                            labelLoading.hide(interfaceDelay);
                                        }
                                    }
                                    return;
                                }
                            }
                        }

                        searchedTexts.push(searchedText);
                        autocompleteInput.data(dataSearchedTexts, searchedTexts);

                        $.ajax(ajax);

                        var loadingThreads = autocompleteInput.data(dataLoadThreads);
                        if (!isArray(loadingThreads)) {
                            loadingThreads = [];
                        }
                        loadingThreads.push(loadingThread);
                        autocompleteInput.data(dataLoadThreads, loadingThreads);
                    }
                }
            }
        }

        var onInput = function() {
            var $this = $(this);

            var loadingThreads = $this.data(dataLoadThreads);
            if (isArrayWithItems(loadingThreads)) {
                for (var loadingThreadId in loadingThreads) {
                    loadingThreads[loadingThreadId].canceled = true;
                }
            }
            $this.data(dataLoadThreads, []);

            if (!$this.is(":focus")) {
                return;
            }

            var timeoutId = $this.data(dataTimeoutId);
            if (timeoutId != null) {
                clearTimeout(timeoutId);
                $this.data(dataTimeoutId, null);
            }

            var choosenVariant = $this.data(dataChoosenVariant);
            if (choosenVariant != null) {
                $this.data(dataChoosenVariant, null);
                return;
            }

            var options = $this.data(dataOptions);
            if ((options == null) || (!isStringWithText(options.checkItemField))) {
                return;
            }

            var searchedText = $this.attr("value");
            if (isString(searchedText)) {
                if (isStringWithText(searchedText)) {
                    searchedText = searchedText.toLowerCase();
                }
            } else {
                searchedText = "";
            }

            if (!isStringWithText(searchedText) && isFunction(options.cleared)) {
                options.cleared();
            }

            var variantsWindow = $this.data(dataVariantWindow);

            if (variantsWindow == null) {
                variantsWindow = $("<div></div>");

                var labelLoading = $("<div></div>")
                    .css({
                        "margin": "5px"
                    })
                    .appendTo(variantsWindow);
                $this.data(dataLabelLoading, labelLoading);

                $("<label>" + options.labelLoading + "</label>")
                    .appendTo(labelLoading);
                $("<hr/>")
                    .css({
                        "color": "#808080"
                    })
                    .appendTo(labelLoading);

                $this.data(dataVariantWindow, variantsWindow);
                variantsWindow.list = $("<ul><ul>")
                        .appendTo(variantsWindow);
            }

            updateVariantsWindow($this);

            if ((options.ajax != null) && isStringWithText(options.ajaxParam)) {
                var timeoutDelay = options.startLoadingDelay;

                var timeoutObject = {
                    autocompleteInput: $this,
                    call: function() {
                        loadItems(this.autocompleteInput);
                    }
                };

                timeoutId = setTimeout(
                    function() {
                        timeoutObject.call();
                    },
                    timeoutDelay
                );
                $this.data(dataTimeoutId, timeoutId);
            }

            var position = $this.offset();

            var height = $this.outerHeight();
            if ((height == null) || (typeof height != typeof 0)) {
                height = 0;
            }
            position.top = position.top + height;

            variantsWindow
                .appendTo($("body"))
                .show()
                .css({
                    "position": "absolute",
                    "display": "block",
                    "height": "auto",
                    "width": "auto",
                    "z-index": "1001",
                    "background-color": "#FFFFFF",
                    "border": "thin solid #D0D0D0",
                    "border-radius": "2px"
                })
                .css(position);
        };

        var onFocusOut = function() {
            var $this = $(this);

            var timeoutId = $this.data(dataTimeoutId);
            if (timeoutId != null) {
                clearTimeout(timeoutId);
                $this.data(dataTimeoutId, null);
            }

            var choosenVariant = $this.data(dataChoosenVariant);
            if (choosenVariant != null) {
                $this.data(dataChoosenVariant, null);
                var options = $this.data(dataOptions);
                if (options != null) {
                    if (isStringWithText(options.checkItemField)) {
                        $this.attr("value", choosenVariant[options.checkItemField]);
                    }
                    if (isFunction(options.itemChoosen)) {
                        options.itemChoosen(choosenVariant);
                    }
                }
            }

            var variantsWindow = $this.data(dataVariantWindow);
            if (variantsWindow != null) {
                variantsWindow.remove();
            }
            $this.data(dataVariantWindow, null);
        };

        return this.each(function() {
            var $this = $(this);

            $this
                .unbind("input", onInput)
                .unbind("dblclick", onInput)
                .unbind("focusout", onFocusOut)
                .attr("value", "")
                .data(dataOptions, options)
                .data(dataVariantWindow, null)
                .data(dataChoosenVariant, null)
                .data(dataTimeoutId, null)
                .data(dataLabelLoading, null)
                .data(dataSearchedTexts, [])
                .data(dataLoadThreads, [])
                .bind("input", onInput)
                .bind("dblclick", onInput)
                .bind("focusout", onFocusOut);
        });
    };
})(jQuery);