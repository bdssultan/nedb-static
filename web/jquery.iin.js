(function($) {
    $.fn.iin = function(options) {
        var settings = $.extend(true, {
            error: function(description) {
                return null;
            },
            success: function() {
                return null;
            },
            trans: {
                wrongLength: "Length is not equal 12",
                notANumber: "Not a number",
                testDigitIsNaN: "Test digit is not a number",
                centuryIsNaN: "Century is not a number",
                yearIsNaN: "Year is not a number",
                monthIsNaN: "Month is not a number",
                dayIsNaN: "Day is not a number",
                invalidBirthDate: "Birth date is invalid",
                finalComparisonFailed: "Final comparison failed"
            }
        }, options);
        
        var checkIIN = function(value) {
            var error = false;
            var description = "";
            
            if ((value == null) || (value.length < 12)) {
                //description = "length != 12";
                description = settings.trans.wrongLength;
                error = true;
            } else if (getInt(value) == null) {
                error = true;
                //description = "not a number";
                description = settings.trans.notANumber;
            } else {
                var testNumber;
                try {
                    testNumber = getInt(value.substring(11, 12));
                } catch (e) {
                    testNumber = null;
                }
                
                error = (testNumber == null);
                if (error) {
                    //description = "Test number is null";
                    description = settings.trans.testDigitIsNaN;
                }
                
                if (!error) {
                    var century;
                    try {
                        century = getInt(value.substring(6,7));
                        if ((century == 1) || (century == 2)) {
                            century = 18;
                        } else if ((century == 3) || (century == 4)) {
                            century = 19;
                        } else if ((century == 5) || (century == 6)) {
                            century = 20;
                        } else {
                            century = null;
                        }
                    } catch (e) {
                        century = null;
                    }
                    
                    error = (century == null);
                    if (error) {
                        //description = "Century is null";
                        //centuryIsNaN: "Century is not a number"
                        description = settings.trans.centuryIsNaN;
                    }
                    
                    if (!error) {
                        var year;
                        try {
                            year = getInt("" + century + value.substring(0, 2));
                        } catch (e) {
                            year = null;
                        }
                        
                        error = (year == null);
                        if (error) {
                            //description = "Year is null";
                            //yearIsNaN: "Year is not a number"
                            description = settings.trans.yearIsNaN;
                        }
                        
                        if (!error) {
                            var month;
                            try {
                                month = getInt(value.substring(2, 4));
                                if ((month < 1) || (month > 12)) {
                                    month = null;
                                }
                            } catch (e) {
                                month = null;
                                error = true;
                            }
                            
                            error = (month == null);
                            if (error) {
                                //description = "Month is null";
                                //monthIsNaN: "Month is not a number"
                                description = settings.trans.monthIsNaN;
                            }
                            
                            if (!error) {
                                var day;
                                try {
                                    day = getInt(value.substring(4, 6));
                                    if ((day < 1) || (day > 31)) {
                                        day = null;
                                    }
                                } catch (e) {
                                    day = null;
                                    error = true;
                                }
                                
                                error = (day == null);
                                if (error) {
                                    //description = "Day is null";
                                    //dayIsNaN: "Day is not a number"
                                    description = settings.trans.dayIsNaN;
                                }
                                
                                if (!error) {
                                    month = month - 1;
                                    
                                    var birthDate;
                                    try {
                                        birthDate = new Date(year, month, day);
                                        
                                        if ((birthDate.getFullYear() != year) || (birthDate.getMonth() != month) || (birthDate.getDate() != day)) {
                                            birthDate = null;
                                        }
                                    } catch (e) {
                                        birthDate = null;
                                    }
                                    
                                    error = (birthDate == null);
                                    if (error) {
                                        //description = "Birth date is null";
                                        //invalidBirthDate: "Birth date is invalid"
                                        description = settings.trans.invalidBirthDate;
                                    }
                                    
                                    if (!error) {
                                        var hzNumber;
                                        try {
                                            hzNumber = getInt(value.substring(7, 11));
                                        } catch (e) {
                                            hzNumber = null;
                                        }
                                        
                                        error = (hzNumber == null);
                                        if (error) {
                                            //description = "HZNumber is null";
                                            //notANumber: "Not a number"
                                            description = settings.trans.notANumber;
                                        }
                                        
                                        if (!error) {
                                            try {
                                                hzNumber = getInt(value.substring(0, 11));
                                            } catch (e) {
                                                hzNumber = null;
                                            }
                                            
                                            error = (hzNumber == null);
                                            if (error) {
                                                //description = "HZNumber (big) is null";
                                                //notANumber: "Not a number"
                                                description = settings.trans.notANumber;
                                            }
                                            
                                            var sum = 0;
                                            for (var i = 0; i < 11; i ++) {
                                                var part;
                                                try {
                                                    part = getInt(value.substring(i, i + 1));
                                                } catch (e) {
                                                    part = null;
                                                }
                                                
                                                error = (part == null);
                                                
                                                if (error) {
                                                    //description = "Part " + i + " is NaN";
                                                    //notANumber
                                                    description = settings.trans.notANumber + " (" + i + ")";
                                                    break;
                                                } else {
                                                    sum = sum + part * (i + 1);
                                                }
                                            }
                                            
                                            if (!error) {
                                                sum = (sum % 11);
                                                if (sum == 10) {
                                                    sum = 0;
                                                    for (var i = 0; i < 11; i ++) {
                                                        var part;
                                                        try {
                                                            part = getInt(value.substring(i, i + 1));
                                                        } catch (e) {
                                                            part = null;
                                                        }
                                                        
                                                        error = (part == null);
                                                        
                                                        if (error) {
                                                            //description = "10 || Part " + i + " is NaN";
                                                            //notANumber
                                                            description = settings.trans.notANumber + " (" + i + ")";
                                                            break;
                                                        } else {
                                                            var weight = (i + 3) % 11;
                                                            if (weight == 0) {
                                                                weight = 11;
                                                            }
                                                            sum = sum + weight * part;
                                                        }
                                                    }
                                                    
                                                    sum = sum % 11;
                                                    
                                                    if (!error) {
                                                        error = ((sum == 10) || (sum != testNumber));
                                                        if (error) {
                                                            //description = "10 || Sum " + sum + ", testNumber " + testNumber;
                                                            //finalComparisonFailed: "Final comparison failed"
                                                            description = settings.trans.finalComparisonFailed;
                                                        }
                                                    }
                                                } else if (sum != testNumber) {
                                                    //description = "Sum " + sum + "; testNumber " + testNumber;
                                                    //finalComparisonFailed: "Final comparison failed"
                                                    description = settings.trans.finalComparisonFailed;
                                                    error = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if (error) {
                return description;
            } else {
                return null;
            }
        };
        
        var getInt = function(text) {
            var value = text;
            
            while ((value.length > 0) && (value.substring(0, 1) == "0")) {
                value = value.substring(1, value.length);
            }
            
            if (value.length == 0) {
                return 0;
            }
            
            var result = parseInt(value);
            value = "" + result;
            
            while (value.length < text.length) {
                value = "0" + value;
            }
            
            if (value == text) {
                return result;
            } else {
                return null;
            }
        };
        
        return $(this).each(function() {
            var error = settings.error;
            var success = settings.success;
            
            var $this = $(this);
            
            $this.mask("999999999999",{
                completed: function() {
                    var iin = $(this).attr("value").replace(/_/, "");
                    var description = checkIIN(iin);
                    
                    if (description == null) {
                        if (success != null) {
                            success();
                        }
                    } else {
                        if (error != null) {
                            error(description);
                        }
                    }
                }
            });
            
            $this.bind("keyup input", function() {
                var description = checkIIN($this.attr("value").replace(/_/, ""));
                
                if (description == null) {
                    if (success != null) {
                        success();
                    }
                } else {
                    if (error != null) {
                        error(description);
                    }
                }
            });
            
            var description = checkIIN($this.attr("value").replace(/_/, ""));
            
            if (description == null) {
                if (success != null) {
                    success();
                }
            } else {
                if (error != null) {
                    error(description);
                }
            }
        });
    }
})(jQuery);