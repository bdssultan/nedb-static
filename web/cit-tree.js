(function($) {
    $(document).ready(function() {
        var createTree = function(container) {
            var $container = $(container);
            
            try {
                var tagName = $container.get(0).tagName.toLowerCase();
                //alert(tagName);
            } catch (e) {
                //alert(container + "\n" + $container + "\n" + e.message);
            }
            
            if (tagName == "td") {
                $container.children("ul").each(function() {
                    createTree(this);
                });
                
                return false;
            } else if (tagName == "li") {
                var hasUL = false;
                var collapse = ($container.children("input:checked").length == 0);
                
                var targets = [];
                
                $container.children("ul").each(function() {
                    targets = targets.concat([$(this)]);
                    hasUL = true;
                    if (createTree(this)) {
                        $(this).hide();
                    } else {
                        collapse = false;
                    }
                });
                
                if (hasUL) {
                    var trigger = null;
                    if (collapse) {
                        trigger = $("<input type=\"button\" value=\"+\"/>");
                    } else {
                        trigger = $("<input type=\"button\" value=\"-\"/>")
                    }
                    trigger.each(function() {
                            this.targets = targets;
                            this.collapsed = collapse;
                        })
                        .prependTo($container)
                        .bind("click", function() {
                            this.collapsed = !this.collapsed;
                            
                            var $this = $(this);
                            
                            if (this.collapsed) {
                                $this.attr("value", "+");
                            } else {
                                $this.attr("value", "-");
                            }
                            
                            for (var index in this.targets) {
                                if (this.collapsed) {
                                    $(this.targets[index]).hide();
                                } else {
                                    $(this.targets[index]).show();
                                }
                            }
                        });
                }
                
                return collapse;
            } else if (tagName == "ul") {
                var collapse = true;
                
                $container.children("li").each(function() {
                    if (!createTree(this)) {
                        collapse = false;
                    }
                });
                
                return collapse;
            }
        }
        
        $("[id|=\"field-boolgroup-tree\"]").each(function() {
            createTree(this);
        });
    });
})(jQuery);