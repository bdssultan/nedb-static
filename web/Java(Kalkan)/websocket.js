/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var webSocket = new WebSocket('wss://127.0.0.1:13579/');
var heartbeat_msg = '--heartbeat--';
var heartbeat_interval = null;
var missed_heartbeats = 0;
var missed_heartbeats_limit_min = 3;
var missed_heartbeats_limit_max = 50;
var missed_heartbeats_limit = missed_heartbeats_limit_min;
var callback = null;


function setMissedHeartbeatsLimitToMax() {
    missed_heartbeats_limit = missed_heartbeats_limit_max;
}

function setMissedHeartbeatsLimitToMin() {
    missed_heartbeats_limit = missed_heartbeats_limit_min;
}

function pingLayer() {
    console.log("pinging...");
    try {
        missed_heartbeats++;
        if (missed_heartbeats >= missed_heartbeats_limit)
            throw new Error("Too many missed heartbeats.");
        webSocket.send(heartbeat_msg);
    } catch (e) {
        clearInterval(heartbeat_interval);
        heartbeat_interval = null;
        setActionLog("Closing connection. Reason: " + e.message);
        console.warn("Closing connection. Reason: " + e.message);
        webSocket.close();
    }
}

webSocket.onopen = function (event) {
    if (heartbeat_interval === null) {
        missed_heartbeats = 0;
        heartbeat_interval = setInterval(pingLayer, 5000);
    }
    console.log("Connection opened");
    moduleIsReady();
};

webSocket.onclose = function (event) {
    if (event.wasClean) {
        console.log('connection has been closed');        
    } else {
        console.log('Connection error');       
    }
    showLogOutput(2,'Для работы модуля подписи необходим <a href="http://pki.gov.kz/index.php/ru/ncalayer">NCALAYER</a>');
    console.log('Code: ' + event.code + ' Reason: ' + event.reason);
};

webSocket.onmessage = function (event) {
    if (event.data === heartbeat_msg) {
        missed_heartbeats = 0;
        return;
    }

    var result = JSON.parse(event.data);

    var rw = {
        result: result['result'],
        secondResult: result['secondResult'],
        errorCode: result['errorCode'],
        getResult: function () {
            return this.result;
        },
        getSecondResult: function () {
            return this.secondResult;
        },
        getErrorCode: function () {
            return this.errorCode;
        }
    };
    if (callback!=null){
    window[callback](rw);}
    console.log(event);
    setMissedHeartbeatsLimitToMin();
};

function jsloaded(){
   
}


function chooseStoragePath(){
    var browseKeyStore = {
        "method": "browseKeyStore",
        "args": [storeType, "P12", ""]
    };
    callback = "chooseStoragePathBack";
    //TODO: CHECK CONNECTION
    setMissedHeartbeatsLimitToMax();
    webSocket.send(JSON.stringify(browseKeyStore));
}


function getKeys(storageName, storagePath, password, type, callBack) {
    var getKeys = {
        "method": "getKeys",
        "args": [storageName, storagePath, password, type]
    };
    callback = callBack;
    setMissedHeartbeatsLimitToMax();
    webSocket.send(JSON.stringify(getKeys));
}

function createCMSSignature(storageName, storagePath, alias, password, dataToSign, attached, callBack) {
    var createCMSSignature = {
        "method": "createCMSSignature",
        "args": [storageName, storagePath, alias, password, dataToSign, attached]
    };
    callback = callBack;
    setMissedHeartbeatsLimitToMax();
    webSocket.send(JSON.stringify(createCMSSignature));
}


function getSubjectDN(storageName, storagePath, alias, password, callBack) {
    var getSubjectDN = {
        "method": "getSubjectDN",
        "args": [storageName, storagePath, alias, password]
    };
    callback = callBack;
    setMissedHeartbeatsLimitToMax();
    webSocket.send(JSON.stringify(getSubjectDN));
}

function getNotBefore(storageName, storagePath, alias, password, callBack) {
    var getNotBefore = {
        "method": "getNotBefore",
        "args": [storageName, storagePath, alias, password]
    };
    callback = callBack;
    setMissedHeartbeatsLimitToMax();
    webSocket.send(JSON.stringify(getNotBefore));
}


function getNotAfter(storageName, storagePath, alias, password, callBack) {
    var getNotAfter = {
        "method": "getNotAfter",
        "args": [storageName, storagePath, alias, password]
    };
    callback = callBack;
    setMissedHeartbeatsLimitToMax();
    webSocket.send(JSON.stringify(getNotAfter));
}

function verifyCMSSignature(sigantureToVerify, signedData, callBack) {
    var verifyCMSSignature = {
        "method": "verifyCMSSignature",
        "args": [sigantureToVerify, signedData]
    };
    callback = callBack;
    setMissedHeartbeatsLimitToMax();
    webSocket.send(JSON.stringify(verifyCMSSignature));
}