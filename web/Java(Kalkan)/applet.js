/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function insertApplet() {
    var app = document.createElement('applet');
    app.id = 'KncaApplet';
    app.name = 'KncaApplet';
    app.archive = '/static/Java(Kalkan)/knca_applet.jar';
    app.code = 'kz.gov.pki.knca.applet.MainApplet';
    app.width = '1';
    app.height = '1';
    app.codebase = ".";
    app.type = "application/x-java-applet";
    app.mayscript = "true";
    var p1 = document.createElement('param');
    p1.name = 'language';
    p1.value = 'ru';
    var p2 = document.createElement('param');
    p2.name = 'separate_jvm';
    p2.value = 'true';
    var p2 = document.createElement('param');
    p2.name = 'codebase_lookup';
    p2.value = 'false';
    app.appendChild(p1);
    app.appendChild(p2);
    document.getElementsByTagName('body')[0].appendChild(app);
}

AppletIsReady = function () {
    moduleIsReady();
};

function jsloaded() {
    insertApplet();
}

function chooseStoragePath(){
    var applet = document.getElementById('KncaApplet');
    var rw = applet.browseKeyStore(storeType, "P12", "");
    chooseStoragePathBack(rw);
}


function getKeys(storageName, storagePath, password, type, callBack) {
   var rw = document.getElementById('KncaApplet').getKeys(storageName, storagePath, password, type);
   getKeysBack(rw);
}
function createCMSSignature(storageName, storagePath, alias, password, dataToSign, attached, callBack) {
    var rw = document.getElementById('KncaApplet').createCMSSignature(storageName, storagePath, alias, password, dataToSign, false);
    createCMSSignatureBack(rw);
}
function getSubjectDN(storageName, storagePath, alias, password, callBack) {
    var rw = document.getElementById('KncaApplet').getSubjectDN(storageName, storagePath, alias, password);
    getSubjectDNBack(rw);
}
function getNotBefore(storageName, storagePath, alias, password, callBack) {
    var rw = document.getElementById('KncaApplet').getNotBefore(storageName, storagePath, alias, password);
    getNotBeforeBack(rw);
}

function getNotAfter(storageName, storagePath, alias, password, callBack) {
    var rw = document.getElementById('KncaApplet').getNotAfter(storageName, storagePath, alias, password);  
    getNotAfterBack(rw);
}

function verifyCMSSignature(sigantureToVerify, signedData, callBack) {
    var rw = document.getElementById('KncaApplet').verifyCMSSignature(sigantureToVerify, signedData);
    verifyCMSSignatureBack(rw);
 }